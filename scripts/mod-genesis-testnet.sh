
change_treasury() {
	jq '.app_state.treasury.reward_weight = "0.900000000000000000" 				  | 
		.app_state.treasury.params.reward_policy.rate_min = "0.90000000000000000" |
		.app_state.treasury.params.reward_policy.rate_max = "0.900000000000000000"' < build/node0/terrad/config/genesis.json >/tmp/genesis.json
	cp /tmp/genesis.json build/node0/terrad/config/genesis.json
	cp /tmp/genesis.json build/node1/terrad/config/genesis.json
	cp /tmp/genesis.json build/node2/terrad/config/genesis.json
	mv /tmp/genesis.json build/node3/terrad/config/genesis.json
}

change_community_tax() {
	jq '.app_state.distribution.params.community_tax = "0.000000000000000000"' <build/node0/terrad/config/genesis.json >/tmp/genesis.json
	cp /tmp/genesis.json build/node0/terrad/config/genesis.json
	cp /tmp/genesis.json build/node1/terrad/config/genesis.json
	cp /tmp/genesis.json build/node2/terrad/config/genesis.json
	mv /tmp/genesis.json build/node3/terrad/config/genesis.json
}

change_nodetoken_to_aztectoken() {
	jq '.app_state.bank.balances[0].coins[0].denom = "caztec" | .app_state.bank.balances[0].coins[0].amount = "25000000" |
		.app_state.bank.balances[1].coins[0].denom = "caztec" | .app_state.bank.balances[1].coins[0].amount = "25000000" |
		.app_state.bank.balances[2].coins[0].denom = "caztec" | .app_state.bank.balances[2].coins[0].amount = "25000000" |
		.app_state.bank.balances[3].coins[0].denom = "caztec" | .app_state.bank.balances[3].coins[0].amount = "25000000"' <build/node0/terrad/config/genesis.json >/tmp/genesis.json
	cp /tmp/genesis.json build/node0/terrad/config/genesis.json
	cp /tmp/genesis.json build/node1/terrad/config/genesis.json
	cp /tmp/genesis.json build/node2/terrad/config/genesis.json
	mv /tmp/genesis.json build/node3/terrad/config/genesis.json
}

change_inflation() {
	jq '.app_state.mint.minter.inflation = "0.0000000"             |
		.app_state.mint.params.inflation_min = "0.0000000"		   |
		.app_state.mint.params.inflation_rate_change = "0.0000000" ' <build/node0/terrad/config/genesis.json >/tmp/genesis.json
	cp /tmp/genesis.json build/node0/terrad/config/genesis.json
	cp /tmp/genesis.json build/node1/terrad/config/genesis.json
	cp /tmp/genesis.json build/node2/terrad/config/genesis.json
	mv /tmp/genesis.json build/node3/terrad/config/genesis.json
}

change_treasury
change_community_tax
change_nodetoken_to_aztectoken
change_inflation