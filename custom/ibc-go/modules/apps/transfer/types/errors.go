package types

import (
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// IBC channel sentinel errors
var (
	ErrInvalidPacketTimeout    = sdkerrors.Register(ModuleName, 2, "invalid packet timeout")
	ErrInvalidDenomForTransfer = sdkerrors.Register(ModuleName, 3, "invalid denomination for cross-chain transfer")
	ErrInvalidVersion          = sdkerrors.Register(ModuleName, 4, "invalid ICS20 version")
	ErrInvalidAmount           = sdkerrors.Register(ModuleName, 5, "invalid token amount")
	ErrTraceNotFound           = sdkerrors.Register(ModuleName, 6, "denomination trace not found")
	ErrSendDisabled            = sdkerrors.Register(ModuleName, 7, "fungible token transfers from this chain are disabled")
	ErrReceiveDisabled         = sdkerrors.Register(ModuleName, 8, "fungible token transfers to this chain are disabled")
	ErrMaxTransferChannels     = sdkerrors.Register(ModuleName, 9, "max transfer channels")
	ErrExitNoCACAO             = sdkerrors.Register(ModuleName, 10, "CACAO has not entered the chain, therefore it cannot leave")
	ErrExitNotEnoughCacao      = sdkerrors.Register(ModuleName, 11, "Can't leave more CACAO than has enter to the chain")
	ErrMaxCACAOReach           = sdkerrors.Register(ModuleName, 12, "The max amount of CACAO on Aztec has already been reach")
	ErrMaxCACAOWillBePass      = sdkerrors.Register(ModuleName, 13, "The max amount of CACAO on Aztec will be pass with that amount, please use less")
)
