package localhost

import (
	"github.com/terra-money/core/custom/ibc-go/modules/light-clients/09-localhost/types"
)

// Name returns the IBC client name
func Name() string {
	return types.SubModuleName
}
