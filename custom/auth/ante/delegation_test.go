package ante_test

import (
	cryptotypes "github.com/cosmos/cosmos-sdk/crypto/types"
	"github.com/cosmos/cosmos-sdk/testutil/testdata"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/terra-money/core/custom/auth/ante"

	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
)

func (suite *AnteTestSuite) TestCheckValidatorMsg() {
	suite.SetupTest(true) // setup
	suite.txBuilder = suite.clientCtx.TxConfig.NewTxBuilder()

	dd := ante.NewDelegationDecorator()
	antehandler := sdk.ChainAnteDecorators(dd)

	// keys, addresses, description
	priv1, pub1, addr1 := testdata.KeyTestPubAddr()

	valAddress := sdk.ValAddress(addr1)
	description := stakingtypes.NewDescription("", "", "", "", "")
	commissionRates := stakingtypes.NewCommissionRates(sdk.NewDec(1), sdk.NewDec(1), sdk.NewDec(1))
	validCommission := sdk.NewDec(1)
	minSelfDelefation := sdk.NewInt(1)
	amount := sdk.NewCoin("uluna", sdk.NewInt(100))
	createValMsg, err := stakingtypes.NewMsgCreateValidator(valAddress, pub1, amount, description, commissionRates, minSelfDelefation)

	// Set IsCheckTx to true
	suite.ctx = suite.ctx.WithIsCheckTx(true)

	// Ok
	suite.ctx = suite.ctx.WithBlockHeight(100)
	suite.Require().NoError(suite.txBuilder.SetMsgs(
		stakingtypes.NewMsgEditValidator(valAddress, description, &validCommission, &minSelfDelefation),
		createValMsg,
	))

	privs, accNums, accSeqs := []cryptotypes.PrivKey{priv1}, []uint64{0}, []uint64{0}
	tx, err := suite.CreateTestTx(privs, accNums, accSeqs, suite.ctx.ChainID())
	suite.Require().NoError(err)

	_, err = antehandler(suite.ctx, tx, false)
	suite.Require().NoError(err)

	// Unauthorized delegation
	suite.ctx = suite.ctx.WithBlockHeight(100)
	suite.Require().NoError(suite.txBuilder.SetMsgs(
		stakingtypes.NewMsgDelegate(addr1, valAddress, amount),
		stakingtypes.NewMsgBeginRedelegate(addr1, sdk.ValAddress(addr1), valAddress, amount),
	))

	tx, err = suite.CreateTestTx(privs, accNums, accSeqs, suite.ctx.ChainID())
	suite.Require().NoError(err)

	suite.ctx = suite.ctx.WithBlockHeight(101)
	_, err = antehandler(suite.ctx, tx, false)
	suite.Require().Error(err)

}
