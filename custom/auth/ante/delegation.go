package ante

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	stakingtypes "github.com/cosmos/cosmos-sdk/x/staking/types"
)

// Commission rate to remove incentives for delegators
var commissionRate = 1.0

// DelegationDecorator will check if the commission rate is set to 100% and
// that the addresses while delegating are the same
type DelegationDecorator struct{}

// NewDelegationDecorator returns new delegation decorator instance
func NewDelegationDecorator() DelegationDecorator {
	return DelegationDecorator{}
}

// AnteHandle handles msg delegation checking
func (dd DelegationDecorator) AnteHandle(ctx sdk.Context, tx sdk.Tx, simulate bool, next sdk.AnteHandler) (newCtx sdk.Context, err error) {
	if !simulate {
		if ctx.IsCheckTx() {
			err := dd.CheckValidatorMsg(ctx, tx.GetMsgs())
			if err != nil {
				return ctx, err
			}
		}
	}

	return next(ctx, tx, simulate)
}

// CheckValidatorMsg prevents delegation to happen by checking that commissionRate = 1.0 and
// checking delegation happens with the same addresses (self delegation)
func (dd DelegationDecorator) CheckValidatorMsg(ctx sdk.Context, msgs []sdk.Msg) error {
	for _, msg := range msgs {
		switch msg := msg.(type) {
		case *stakingtypes.MsgCreateValidator:
			if msg.Commission.Rate.MustFloat64() != commissionRate {
				return sdkerrors.Wrapf(sdkerrors.ErrUnauthorized, "Invalid commission rate; got %f, required %f", msg.Commission.Rate.MustFloat64(), commissionRate)
			}

		case *stakingtypes.MsgEditValidator:
			if msg.CommissionRate.MustFloat64() != commissionRate {
				return sdkerrors.Wrapf(sdkerrors.ErrUnauthorized, "Invalid commission rate; got %f, required %f", msg.CommissionRate.MustFloat64(), commissionRate)
			}

		case *stakingtypes.MsgDelegate:
			if msg.ValidatorAddress != msg.DelegatorAddress {
				return sdkerrors.Wrapf(sdkerrors.ErrUnauthorized, "Unauthorized, cannot delegate. Just self delegation allowed")
			}

		case *stakingtypes.MsgBeginRedelegate:
			if msg.ValidatorDstAddress != msg.DelegatorAddress {
				return sdkerrors.Wrapf(sdkerrors.ErrUnauthorized, "Unauthorized, cannot redelegate to another address. Just self delegation allowed")
			}
		}
	}
	return nil
}
