package assets

//nolint
const (
	MicroLunaDenom  = "uluna"
	MicroUSDDenom   = "uusd"
	MicroKRWDenom   = "ukrw"
	MicroSDRDenom   = "usdr"
	MicroCNYDenom   = "ucny"
	MicroJPYDenom   = "ujpy"
	MicroEURDenom   = "ueur"
	MicroGBPDenom   = "ugbp"
	MicroMNTDenom   = "umnt"
	MicroUSbDenom   = "uusb"
	MicroUSmDenom   = "uusm"
	MicroUSaDenom   = "uusa"
	CentiAztecDenom = "caztec"

	MicroUnit = int64(1e6)
)
