package keeper

import (
	"math/rand"
	"testing"

	core "github.com/terra-money/core/types"

	"github.com/stretchr/testify/require"

	sdk "github.com/cosmos/cosmos-sdk/types"
	treasurytypes "github.com/terra-money/core/x/treasury/types"
)

func TestSettle(t *testing.T) {
	input := CreateTestInput(t)

	faucetBalance := input.BankKeeper.GetBalance(input.Ctx, input.AccountKeeper.GetModuleAddress(faucetAccountName), core.MicroLunaDenom)
	burnAmt := sdk.NewInt(rand.Int63()%faucetBalance.Amount.Int64() + 1)
	initialLunaSupply := input.BankKeeper.GetSupply(input.Ctx, core.MicroLunaDenom)
	input.TreasuryKeeper.RecordEpochInitialIssuance(input.Ctx)

	input.Ctx = input.Ctx.WithBlockHeight(int64(core.BlocksPerWeek))
	err := input.BankKeeper.BurnCoins(input.Ctx, faucetAccountName, sdk.NewCoins(sdk.NewCoin(core.MicroLunaDenom, burnAmt)))
	require.NoError(t, err)

	// check seigniorage update
	require.Equal(t, burnAmt, input.TreasuryKeeper.PeekEpochSeigniorage(input.Ctx))

	input.TreasuryKeeper.SettleSeigniorage(input.Ctx)
	lunaSupply := input.BankKeeper.GetSupply(input.Ctx, core.MicroLunaDenom)
	feePool := input.DistrKeeper.GetFeePool(input.Ctx)

	// Reward weight portion of seigniorage burned
	rewardWeight := input.TreasuryKeeper.GetRewardWeight(input.Ctx)
	leftAmt := burnAmt.Sub(rewardWeight.MulInt(burnAmt).TruncateInt())

	require.Equal(t, lunaSupply.Amount, initialLunaSupply.Amount.Sub(burnAmt).Add(leftAmt))
	require.Equal(t, leftAmt, feePool.CommunityPool.AmountOf(core.MicroLunaDenom).TruncateInt())
}

func TestOneRewardWeightSettle(t *testing.T) {
	input := CreateTestInput(t)

	// set zero reward weight
	input.TreasuryKeeper.SetRewardWeight(input.Ctx, sdk.OneDec())

	faucetBalance := input.BankKeeper.GetBalance(input.Ctx, input.AccountKeeper.GetModuleAddress(faucetAccountName), core.MicroLunaDenom)
	burnAmt := sdk.NewInt(rand.Int63()%faucetBalance.Amount.Int64() + 1)
	initialLunaSupply := input.BankKeeper.GetSupply(input.Ctx, core.MicroLunaDenom)
	input.TreasuryKeeper.RecordEpochInitialIssuance(input.Ctx)

	input.Ctx = input.Ctx.WithBlockHeight(int64(core.BlocksPerWeek))
	err := input.BankKeeper.BurnCoins(input.Ctx, faucetAccountName, sdk.NewCoins(sdk.NewCoin(core.MicroLunaDenom, burnAmt)))
	require.NoError(t, err)

	// check seigniorage update
	require.Equal(t, burnAmt, input.TreasuryKeeper.PeekEpochSeigniorage(input.Ctx))

	input.TreasuryKeeper.SettleSeigniorage(input.Ctx)
	lunaSupply := input.BankKeeper.GetSupply(input.Ctx, core.MicroLunaDenom)
	feePool := input.DistrKeeper.GetFeePool(input.Ctx)

	// Reward weight portion of seigniorage burned
	require.Equal(t, lunaSupply.Amount, initialLunaSupply.Amount.Sub(burnAmt))
	require.Equal(t, sdk.ZeroInt(), feePool.CommunityPool.AmountOf(core.MicroLunaDenom).TruncateInt())
}

func TestDistributeAztecFund(t *testing.T) {
	input := CreateTestInput(t)

	//Set the AztecFund with 100,000,000 uluna
	coin := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(100000000))
	err := input.BankKeeper.SendCoinsFromModuleToModule(input.Ctx, faucetAccountName, treasurytypes.AztecFund, sdk.NewCoins(coin))
	require.Equal(t, nil, err)

	//Set accounts for TestDistributeAztecFund
	addr1 := GetRandomBech32Addr()
	addr2 := GetRandomBech32Addr()

	acc1 := input.AccountKeeper.NewAccountWithAddress(input.Ctx, addr1)
	acc2 := input.AccountKeeper.NewAccountWithAddress(input.Ctx, addr2)

	//Add aztec token to accounts -> acc1 = 75%, acc2 = 25%
	acc1Coin := sdk.NewCoin(core.CentiAztecDenom, sdk.NewInt(75000000))
	acc2Coin := sdk.NewCoin(core.CentiAztecDenom, sdk.NewInt(25000000))

	err = input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, faucetAccountName, acc1.GetAddress(), sdk.NewCoins(acc1Coin))
	require.Equal(t, nil, err)
	err = input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, faucetAccountName, acc2.GetAddress(), sdk.NewCoins(acc2Coin))
	require.Equal(t, nil, err)

	input.TreasuryKeeper.DistributeAztecFund(input.Ctx)

	//Distribute AztecFund -> acc1 with 75% and acc2 with 25%
	balAcc1 := sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc1.GetAddress(), core.MicroLunaDenom))
	balAcc2 := sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc2.GetAddress(), core.MicroLunaDenom))
	balAztec := sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, input.AccountKeeper.GetModuleAddress(treasurytypes.AztecFund), core.MicroLunaDenom))

	for _, coin := range balAcc1 {
		if coin.GetDenom() == core.MicroLunaDenom {
			require.Equal(t, true, coin.Amount.Equal(sdk.NewInt(75000000)))
		}
	}

	for _, coin := range balAcc2 {
		if coin.GetDenom() == core.MicroLunaDenom {
			require.Equal(t, true, coin.Amount.Equal(sdk.NewInt(25000000)))
		}
	}

	//At the end the AztecFund should be left with 0 uluna
	for _, coin := range balAztec {
		if coin.GetDenom() == core.MicroLunaDenom {
			require.Equal(t, true, coin.Amount.Equal(sdk.NewInt(0)))
		}
	}

	newAztec := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(987654300))
	err = input.BankKeeper.SendCoinsFromModuleToModule(input.Ctx, faucetAccountName, treasurytypes.AztecFund, sdk.NewCoins(newAztec))
	require.Equal(t, nil, err)

	input.TreasuryKeeper.DistributeAztecFund(input.Ctx)

	balAcc1 = sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc1.GetAddress(), core.MicroLunaDenom))
	balAcc2 = sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc2.GetAddress(), core.MicroLunaDenom))
	balAztec = sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, input.AccountKeeper.GetModuleAddress(treasurytypes.AztecFund), core.MicroLunaDenom))

	//Distribute AztecFund -> acc1 with 75% and acc2 with 25%
	for _, coin := range balAcc1 {
		if coin.GetDenom() == core.MicroLunaDenom {
			require.Equal(t, true, coin.Amount.Equal(sdk.NewInt(75000000+675000000)))
		}
	}

	for _, coin := range balAcc2 {
		if coin.GetDenom() == core.MicroLunaDenom {
			require.Equal(t, true, coin.Amount.Equal(sdk.NewInt(25000000+225000000)))
		}
	}

	//At the end the AztecFund should be left with 876,543 uluna
	for _, coin := range balAztec {
		if coin.GetDenom() == core.MicroLunaDenom {
			require.Equal(t, true, coin.Amount.Equal(sdk.NewInt(87654300)))
		}
	}
}

func TestDistributeNodeSeigniorage(t *testing.T) {
	input, _ := setupValidators((t))

	//Mint coins to treasury module and add 2 validators
	err := input.BankKeeper.MintCoins(input.Ctx, treasurytypes.ModuleName, sdk.NewCoins(sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(100000000))))
	require.Equal(t, err, nil)
	validators := input.StakingKeeper.GetAllValidators(input.Ctx)

	//A total of 10,000,000 with 2 validators, each should get 5,000,000
	coinAmt := sdk.NewInt(10000000)
	var prevBalance []sdk.Coin
	amt := sdk.NewInt(5000000)

	for _, val := range validators {
		prevBalance = append(prevBalance, input.BankKeeper.GetBalance(input.Ctx, val.GetOperator().Bytes(), core.MicroLunaDenom))
	}
	input.TreasuryKeeper.DistributeNodeSeigniorage(input.Ctx, coinAmt)
	for num, val := range validators {
		currentBalance := input.BankKeeper.GetBalance(input.Ctx, val.GetOperator().Bytes(), core.MicroLunaDenom)
		require.Equal(t, true, currentBalance.Equal(prevBalance[num].Add(sdk.NewCoin(core.MicroLunaDenom, amt))))
	}

	//A total of 5,000,000 with 2 validators, each should get 2,500,000
	coinAmt = sdk.NewInt(5000000)
	amt = sdk.NewInt(2500000)
	prevBalance = nil

	for _, val := range validators {
		prevBalance = append(prevBalance, input.BankKeeper.GetBalance(input.Ctx, val.GetOperator().Bytes(), core.MicroLunaDenom))
	}
	input.TreasuryKeeper.DistributeNodeSeigniorage(input.Ctx, coinAmt)
	for num, val := range validators {
		currentBalance := input.BankKeeper.GetBalance(input.Ctx, val.GetOperator().Bytes(), core.MicroLunaDenom)
		require.Equal(t, true, currentBalance.Equal(prevBalance[num].Add(sdk.NewCoin(core.MicroLunaDenom, amt))))
	}

}
