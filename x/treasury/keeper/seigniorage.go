package keeper

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"

	core "github.com/terra-money/core/types"
	"github.com/terra-money/core/x/treasury/types"
)

// SettleSeigniorage computes seigniorage and distributes it to oracle and distribution(community-pool) account
func (k Keeper) SettleSeigniorage(ctx sdk.Context) {
	// Mint seigniorage for oracle and community pool
	seigniorageLunaAmt := k.PeekEpochSeigniorage(ctx)
	if seigniorageLunaAmt.LTE(sdk.ZeroInt()) {
		return
	}

	// Settle current epoch seigniorage
	// {90% of the total -> 100%} = 90/100
	rewardWeight := k.GetRewardWeight(ctx)

	// Align seigniorage to usdr
	seigniorageDecCoin := sdk.NewDecCoin(core.MicroLunaDenom, seigniorageLunaAmt)

	// Mint seigniorage
	seigniorageCoin, _ := seigniorageDecCoin.TruncateDecimal()
	seigniorageCoins := sdk.NewCoins(seigniorageCoin)
	err := k.bankKeeper.MintCoins(ctx, types.ModuleName, seigniorageCoins)
	if err != nil {
		panic(err)
	}
	seigniorageAmt := seigniorageCoin.Amount

	// Send reward to oracle module
	burnAmt := rewardWeight.MulInt(seigniorageAmt).TruncateInt()
	burnCoins := sdk.NewCoins(sdk.NewCoin(core.MicroLunaDenom, burnAmt))
	err = k.bankKeeper.BurnCoins(ctx, types.ModuleName, burnCoins)
	if err != nil {
		panic(err)
	}

	// Send left to validators and AztecFund
	leftAmt := seigniorageAmt.Sub(burnAmt)
	communityPoolCoins := sdk.NewCoins(sdk.NewCoin(core.MicroLunaDenom, leftAmt))

	err = k.bankKeeper.SendCoinsFromModuleToModule(ctx, types.ModuleName, k.distributionModuleName, communityPoolCoins)
	if err != nil {
		panic(err)
	}

	// Update distribution community pool
	feePool := k.distrKeeper.GetFeePool(ctx)
	feePool.CommunityPool = feePool.CommunityPool.Add(sdk.NewDecCoinsFromCoins(communityPoolCoins...)...)
	k.distrKeeper.SetFeePool(ctx, feePool)
}

//Distribute the tokens that are in the AztecFund
func (k Keeper) DistributeAztecFund(ctx sdk.Context) {
	accounts := k.accountKeeper.GetAllAccounts(ctx)
	aztecFundAddr := k.accountKeeper.GetModuleAddress(types.AztecFund)
	aztecFundBalance := k.bankKeeper.GetAllBalances(ctx, aztecFundAddr)
	totalAmountOfAztec := k.bankKeeper.GetSupply(ctx, core.CentiAztecDenom).Amount.ToDec()

	//Iterate from all the available tokens from the Aztec Fund
	for _, token := range aztecFundBalance {

		//Distribute only if the amount of token is at least MinMultipleAztecFund
		if token.IsGTE(sdk.NewCoin(token.GetDenom(), sdk.NewIntFromUint64(core.MinMultipleAztecFund))) {

			//Iterate for all the available accounts
			for _, acc := range accounts {

				//Get the amount of AztecToken from the account
				aztecBalance := k.bankKeeper.GetBalance(ctx, acc.GetAddress(), core.CentiAztecDenom)

				// Check if account has AztecToken
				if !aztecBalance.IsZero() {
					tokenMultiplier := (token.Amount.Quo(sdk.NewIntFromUint64(core.MinMultipleAztecFund))).Mul(sdk.NewIntFromUint64(core.MinMultipleAztecFund))
					tokenMillion := sdk.NewDecCoin(token.Denom, tokenMultiplier)
					aztecAmount := aztecBalance.Amount.ToDec()
					percentage := aztecAmount.Quo(totalAmountOfAztec)
					percentageCoins := tokenMillion.Amount.Mul(percentage)

					sendCoin, _ := sdk.NewDecCoinFromDec(token.Denom, percentageCoins).TruncateDecimal()

					err := k.bankKeeper.SendCoinsFromModuleToAccount(ctx, types.AztecFund, acc.GetAddress(), sdk.NewCoins(sendCoin))
					if err != nil {
						ctx.Logger().Error("fail to send", token.Denom, " on AztecFund", "error", err)
					}
				}
			}
		}
	}
}

//Distribute the amount to the validators of the chain.
//Currently not used, left in case of need it.
func (k Keeper) DistributeNodeSeigniorage(ctx sdk.Context, nodeCoinsAmt sdk.Int) {

	var valAddrs []sdk.ValAddress
	numOfVals := 0
	maxValidators := k.stakingKeeper.MaxValidators(ctx)
	iterator := k.stakingKeeper.ValidatorsPowerStoreIterator(ctx)
	defer iterator.Close()

	//Get validators addresses
	for ; iterator.Valid() && numOfVals < int(maxValidators); iterator.Next() {
		validator := k.stakingKeeper.Validator(ctx, iterator.Value())

		valAddr := validator.GetOperator()
		valAddrs = append(valAddrs, valAddr)
		numOfVals++
	}

	//Distribute only if there are validators
	if numOfVals != 0 {
		//Set the amount to distribute
		amountPerVal := nodeCoinsAmt.Quo(sdk.NewInt(int64(numOfVals)))
		coinsPerVal := sdk.NewCoins(sdk.NewCoin(core.MicroLunaDenom, amountPerVal))

		for _, addr := range valAddrs {
			err := k.bankKeeper.SendCoinsFromModuleToAccount(ctx, types.ModuleName, addr.Bytes(), coinsPerVal)
			if err != nil {
				sdkerrors.Wrap(err, "DistributeNodeSeigniorage error")
			}
		}
	}
}
