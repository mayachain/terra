package market

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	core "github.com/terra-money/core/types"

	"github.com/terra-money/core/x/market/keeper"
)

var DistributeCACAOBondReserveNextBlock = false

// EndBlocker is called at the end of every block
func EndBlocker(ctx sdk.Context, k keeper.Keeper) {

	// Replenishes each pools towards equilibrium
	k.ReplenishPools(ctx)

	if DistributeCACAOBondReserveNextBlock {
		k.DistributeUSbBondReserve(ctx)
		DistributeCACAOBondReserveNextBlock = false
	}

	if core.IsPeriodLastBlock(ctx, core.BlocksPerDay) {
		k.DistributeCACAOBondReserve(ctx)
		DistributeCACAOBondReserveNextBlock = true
	}
}
