package keeper

import (
	"testing"

	"github.com/stretchr/testify/require"

	core "github.com/terra-money/core/types"
	"github.com/terra-money/core/x/market/types"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

func TestTerraPoolDeltaUpdate(t *testing.T) {
	input := CreateTestInput(t)

	terraPoolDelta := input.MarketKeeper.GetTerraPoolDelta(input.Ctx)
	require.Equal(t, sdk.ZeroDec(), terraPoolDelta)

	diff := sdk.NewDec(10)
	input.MarketKeeper.SetTerraPoolDelta(input.Ctx, diff)

	terraPoolDelta = input.MarketKeeper.GetTerraPoolDelta(input.Ctx)
	require.Equal(t, diff, terraPoolDelta)
}

// TestReplenishPools tests that
// each pools move towards base pool
func TestReplenishPools(t *testing.T) {
	input := CreateTestInput(t)
	input.OracleKeeper.SetLunaExchangeRate(input.Ctx, core.MicroSDRDenom, sdk.OneDec())

	basePool := input.MarketKeeper.BasePool(input.Ctx)
	terraPoolDelta := input.MarketKeeper.GetTerraPoolDelta(input.Ctx)
	require.True(t, terraPoolDelta.IsZero())

	// Positive delta
	diff := basePool.QuoInt64((int64)(core.BlocksPerDay))
	input.MarketKeeper.SetTerraPoolDelta(input.Ctx, diff)

	input.MarketKeeper.ReplenishPools(input.Ctx)

	terraPoolDelta = input.MarketKeeper.GetTerraPoolDelta(input.Ctx)
	replenishAmt := diff.QuoInt64((int64)(input.MarketKeeper.PoolRecoveryPeriod(input.Ctx)))
	expectedDelta := diff.Sub(replenishAmt)
	require.Equal(t, expectedDelta, terraPoolDelta)

	// Negative delta
	diff = diff.Neg()
	input.MarketKeeper.SetTerraPoolDelta(input.Ctx, diff)

	input.MarketKeeper.ReplenishPools(input.Ctx)

	terraPoolDelta = input.MarketKeeper.GetTerraPoolDelta(input.Ctx)
	replenishAmt = diff.QuoInt64((int64)(input.MarketKeeper.PoolRecoveryPeriod(input.Ctx)))
	expectedDelta = diff.Sub(replenishAmt)
	require.Equal(t, expectedDelta, terraPoolDelta)
}

func TestAddStakeUSbAndSubStakeUSb(t *testing.T) {
	input := CreateTestInput(t)
	accounts := input.AccountKeeper.GetAllAccounts(input.Ctx)
	coin := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(1000))

	acc := accounts[0]
	acc2 := accounts[1]

	input.BankKeeper.MintCoins(input.Ctx, types.ModuleName, sdk.NewCoins(coin.Add(coin)))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc.GetAddress(), sdk.NewCoins(coin))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc2.GetAddress(), sdk.NewCoins(coin))

	err := input.MarketKeeper.AddStakeUSb(input.Ctx, coin, acc.GetAddress())
	require.NoError(t, err)
	err = input.MarketKeeper.AddStakeUSb(input.Ctx, coin, acc2.GetAddress())
	require.NoError(t, err)

	expected := []types.StakeUSbParams{}
	newParam := types.StakeUSbParams{}

	newParam.StakeAddress = acc.GetAddress().String()
	newParam.UsbStake = &coin
	expected = append(expected, newParam)

	newParam.StakeAddress = acc2.GetAddress().String()
	newParam.UsbStake = &coin
	expected = append(expected, newParam)

	require.Equal(t, expected, input.MarketKeeper.GetStakeUSb(input.Ctx))

	err = input.MarketKeeper.SubStakeUSb(input.Ctx, coin, acc.GetAddress())
	require.NoError(t, err)
	err = input.MarketKeeper.SubStakeUSb(input.Ctx, coin, acc2.GetAddress())
	require.NoError(t, err)

	expected = nil
	require.Equal(t, expected, input.MarketKeeper.GetStakeUSb(input.Ctx))
}

func TestConvertUSaToUSb(t *testing.T) {
	input := CreateTestInput(t)

	inCoin := sdk.NewCoin(core.MicroUSaDenom, sdk.NewInt(1000))
	outUSb := input.MarketKeeper.ConvertUSaToUSb(inCoin)
	expectedUSb := sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(990))

	require.Equal(t, expectedUSb, outUSb)

	inCoin = sdk.NewCoin(core.MicroUSaDenom, sdk.NewInt(500))
	outUSb = input.MarketKeeper.ConvertUSaToUSb(inCoin)
	expectedUSb = sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(495))

	require.Equal(t, expectedUSb, outUSb)

	inCoin = sdk.NewCoin(core.MicroUSaDenom, sdk.NewInt(100))
	outUSb = input.MarketKeeper.ConvertUSaToUSb(inCoin)
	expectedUSb = sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(99))

	require.Equal(t, expectedUSb, outUSb)
}

func TestConvertCACAOToUSb(t *testing.T) {
	input := CreateTestInput(t)
	CACAOToUSD := sdk.NewDecWithPrec(95, 2)
	CACAOToUSD = CACAOToUSD.Add(sdk.OneDec())

	input.OracleKeeper.SetLunaExchangeRate(input.Ctx, core.MicroUSDDenom, CACAOToUSD)

	inCoin := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(1000))
	outUSb := input.MarketKeeper.ConvertCACAOToUSb(input.Ctx, inCoin)
	expectedUSb := sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(1950))

	require.Equal(t, expectedUSb, outUSb)

	inCoin = sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(500))
	outUSb = input.MarketKeeper.ConvertCACAOToUSb(input.Ctx, inCoin)
	expectedUSb = sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(975))

	require.Equal(t, expectedUSb, outUSb)

	inCoin = sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(100))
	outUSb = input.MarketKeeper.ConvertCACAOToUSb(input.Ctx, inCoin)
	expectedUSb = sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(195))

	require.Equal(t, expectedUSb, outUSb)
}

func TestMintCoinIntoBondReserve(t *testing.T) {
	input := CreateTestInput(t)

	addrBondReserve := input.AccountKeeper.GetModuleAddress(types.BondReserve)
	balanceBondReserve := input.BankKeeper.GetBalance(input.Ctx, addrBondReserve, core.MicroLunaDenom)
	expected := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(0))

	require.Equal(t, expected, balanceBondReserve)

	newCoin := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(100))
	input.MarketKeeper.MintCoinIntoBondReserve(input.Ctx, newCoin)
	balanceBondReserve = input.BankKeeper.GetBalance(input.Ctx, addrBondReserve, core.MicroLunaDenom)
	expected = newCoin

	require.Equal(t, expected, balanceBondReserve)

	newCoin = sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(564))
	input.MarketKeeper.MintCoinIntoBondReserve(input.Ctx, newCoin)
	balanceBondReserve = input.BankKeeper.GetBalance(input.Ctx, addrBondReserve, core.MicroLunaDenom)
	expected = expected.Add(newCoin)

	require.Equal(t, expected, balanceBondReserve)

	newCoin = sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(125))
	input.MarketKeeper.MintCoinIntoBondReserve(input.Ctx, newCoin)
	balanceBondReserve = input.BankKeeper.GetBalance(input.Ctx, addrBondReserve, core.MicroLunaDenom)
	expected = expected.Add(newCoin)

	require.Equal(t, expected, balanceBondReserve)
}

func TestDistributeCACAOBondReserve(t *testing.T) {
	input := CreateTestInput(t)
	accounts := input.AccountKeeper.GetAllAccounts(input.Ctx)
	input.OracleKeeper.SetLunaExchangeRate(input.Ctx, core.MicroUSDDenom, sdk.OneDec())
	lunaCoin := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(100000000))
	USbCoin := sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(10000000000))

	acc := accounts[0]
	acc2 := accounts[1]
	acc3 := accounts[2]
	acc4 := accounts[3]

	//Remove balance from accounts
	input.BankKeeper.SendCoinsFromAccountToModule(input.Ctx, acc.GetAddress(), types.ModuleName, sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc.GetAddress(), core.MicroLunaDenom)))
	input.BankKeeper.SendCoinsFromAccountToModule(input.Ctx, acc2.GetAddress(), types.ModuleName, sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc2.GetAddress(), core.MicroLunaDenom)))
	input.BankKeeper.SendCoinsFromAccountToModule(input.Ctx, acc3.GetAddress(), types.ModuleName, sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc3.GetAddress(), core.MicroLunaDenom)))
	input.BankKeeper.SendCoinsFromAccountToModule(input.Ctx, acc4.GetAddress(), types.ModuleName, sdk.NewCoins(input.BankKeeper.GetBalance(input.Ctx, acc4.GetAddress(), core.MicroLunaDenom)))

	input.BankKeeper.MintCoins(input.Ctx, types.ModuleName, sdk.NewCoins(lunaCoin))
	input.BankKeeper.SendCoinsFromModuleToModule(input.Ctx, types.ModuleName, types.BondReserve, sdk.NewCoins(lunaCoin))

	input.BankKeeper.MintCoins(input.Ctx, types.ModuleName, sdk.NewCoins(USbCoin.Add(USbCoin)))
	input.BankKeeper.MintCoins(input.Ctx, types.ModuleName, sdk.NewCoins(USbCoin.Add(USbCoin)))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc.GetAddress(), sdk.NewCoins(USbCoin))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc2.GetAddress(), sdk.NewCoins(USbCoin))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc3.GetAddress(), sdk.NewCoins(USbCoin))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc4.GetAddress(), sdk.NewCoins(USbCoin))

	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoin, acc.GetAddress())
	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoin, acc2.GetAddress())
	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoin, acc3.GetAddress())
	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoin, acc4.GetAddress())

	input.MarketKeeper.DistributeCACAOBondReserve(input.Ctx)

	accBal := input.BankKeeper.GetBalance(input.Ctx, acc.GetAddress(), core.MicroLunaDenom)
	acc2Bal := input.BankKeeper.GetBalance(input.Ctx, acc2.GetAddress(), core.MicroLunaDenom)
	acc3Bal := input.BankKeeper.GetBalance(input.Ctx, acc3.GetAddress(), core.MicroLunaDenom)
	acc4Bal := input.BankKeeper.GetBalance(input.Ctx, acc4.GetAddress(), core.MicroLunaDenom)
	expected := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(25000000))

	require.Equal(t, expected, accBal)
	require.Equal(t, expected, acc2Bal)
	require.Equal(t, expected, acc3Bal)
	require.Equal(t, expected, acc4Bal)

	stakers := input.MarketKeeper.GetStakeUSb(input.Ctx)
	accBal = *stakers[0].UsbStake
	acc2Bal = *stakers[1].UsbStake
	acc3Bal = *stakers[2].UsbStake
	acc4Bal = *stakers[3].UsbStake
	expected = sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(10000000000-25000000))

	require.Equal(t, expected, accBal)
	require.Equal(t, expected, acc2Bal)
	require.Equal(t, expected, acc3Bal)
	require.Equal(t, expected, acc4Bal)
}

func TestDistributeUSbBondReserve(t *testing.T) {
	input := CreateTestInput(t)
	accounts := input.AccountKeeper.GetAllAccounts(input.Ctx)
	input.OracleKeeper.SetLunaExchangeRate(input.Ctx, core.MicroUSDDenom, sdk.OneDec())
	USbCoin := sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(10000000000))
	USbCoinHalf := sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(5000000000))
	USbMintedPerAcc := sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(1000000))

	acc := accounts[0]
	acc2 := accounts[1]
	acc3 := accounts[2]
	acc4 := accounts[3]

	input.BankKeeper.MintCoins(input.Ctx, types.ModuleName, sdk.NewCoins(USbCoin.Add(USbCoin)))
	input.BankKeeper.MintCoins(input.Ctx, types.ModuleName, sdk.NewCoins(USbCoin.Add(USbCoin)))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc.GetAddress(), sdk.NewCoins(USbCoin))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc2.GetAddress(), sdk.NewCoins(USbCoin))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc3.GetAddress(), sdk.NewCoins(USbCoin))
	input.BankKeeper.SendCoinsFromModuleToAccount(input.Ctx, types.ModuleName, acc4.GetAddress(), sdk.NewCoins(USbCoin))

	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoinHalf, acc.GetAddress())
	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoinHalf, acc2.GetAddress())
	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoinHalf, acc3.GetAddress())
	input.MarketKeeper.AddStakeUSb(input.Ctx, USbCoinHalf, acc4.GetAddress())

	input.MarketKeeper.DistributeUSbBondReserve(input.Ctx)

	accBal := input.BankKeeper.GetBalance(input.Ctx, acc.GetAddress(), core.MicroUSbDenom)
	acc2Bal := input.BankKeeper.GetBalance(input.Ctx, acc2.GetAddress(), core.MicroUSbDenom)
	acc3Bal := input.BankKeeper.GetBalance(input.Ctx, acc3.GetAddress(), core.MicroUSbDenom)
	acc4Bal := input.BankKeeper.GetBalance(input.Ctx, acc4.GetAddress(), core.MicroUSbDenom)
	expected := USbCoinHalf.Add(USbMintedPerAcc)

	require.Equal(t, expected, accBal)
	require.Equal(t, expected, acc2Bal)
	require.Equal(t, expected, acc3Bal)
	require.Equal(t, expected, acc4Bal)
}
