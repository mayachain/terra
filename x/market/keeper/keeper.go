package keeper

import (
	"fmt"

	"github.com/tendermint/tendermint/libs/log"

	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	paramstypes "github.com/cosmos/cosmos-sdk/x/params/types"
	core "github.com/terra-money/core/types"

	"github.com/terra-money/core/x/market/types"
)

// Keeper of the market store
type Keeper struct {
	storeKey   sdk.StoreKey
	cdc        codec.BinaryCodec
	paramSpace paramstypes.Subspace

	AccountKeeper types.AccountKeeper
	BankKeeper    types.BankKeeper
	OracleKeeper  types.OracleKeeper
}

// NewKeeper constructs a new keeper for oracle
func NewKeeper(
	cdc codec.BinaryCodec,
	storeKey sdk.StoreKey,
	paramstore paramstypes.Subspace,
	accountKeeper types.AccountKeeper,
	bankKeeper types.BankKeeper,
	oracleKeeper types.OracleKeeper,
) Keeper {

	// ensure market module account is set
	if addr := accountKeeper.GetModuleAddress(types.ModuleName); addr == nil {
		panic(fmt.Sprintf("%s module account has not been set", types.ModuleName))
	}

	// set KeyTable if it has not already been set
	if !paramstore.HasKeyTable() {
		paramstore = paramstore.WithKeyTable(types.ParamKeyTable())
	}

	return Keeper{
		cdc:           cdc,
		storeKey:      storeKey,
		paramSpace:    paramstore,
		AccountKeeper: accountKeeper,
		BankKeeper:    bankKeeper,
		OracleKeeper:  oracleKeeper,
	}
}

// Logger returns a module-specific logger.
func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

// GetTerraPoolDelta returns the gap between the TerraPool and the TerraBasePool
func (k Keeper) GetTerraPoolDelta(ctx sdk.Context) sdk.Dec {
	store := ctx.KVStore(k.storeKey)
	bz := store.Get(types.TerraPoolDeltaKey)
	if bz == nil {
		return sdk.ZeroDec()
	}

	dp := sdk.DecProto{}
	k.cdc.MustUnmarshal(bz, &dp)
	return dp.Dec
}

// SetTerraPoolDelta updates TerraPoolDelta which is gap between the TerraPool and the BasePool
func (k Keeper) SetTerraPoolDelta(ctx sdk.Context, delta sdk.Dec) {
	store := ctx.KVStore(k.storeKey)
	bz := k.cdc.MustMarshal(&sdk.DecProto{Dec: delta})
	store.Set(types.TerraPoolDeltaKey, bz)
}

// ReplenishPools replenishes each pool(Terra,Luna) to BasePool
func (k Keeper) ReplenishPools(ctx sdk.Context) {
	poolDelta := k.GetTerraPoolDelta(ctx)

	poolRecoveryPeriod := int64(k.PoolRecoveryPeriod(ctx))
	poolRegressionAmt := poolDelta.QuoInt64(poolRecoveryPeriod)

	// Replenish pools towards each base pool
	// regressionAmt cannot make delta zero
	poolDelta = poolDelta.Sub(poolRegressionAmt)

	k.SetTerraPoolDelta(ctx, poolDelta)
}

// GetBondReserve returns Coins on the BondReserve
func (k Keeper) GetBondReserve(ctx sdk.Context) sdk.Coins {
	coins := k.BankKeeper.GetAllBalances(ctx, k.AccountKeeper.GetModuleAddress(types.BondReserve))
	return coins
}

// GetStakeUSb returns staked $USb into the BondReserve
func (k Keeper) GetStakeUSb(ctx sdk.Context) (stake []types.StakeUSbParams) {
	store := ctx.KVStore(k.storeKey)

	bz := store.Get(types.StakeUSbKey)
	if bz == nil {
		return []types.StakeUSbParams{}
	}
	st := types.StakeUSb{}
	k.cdc.MustUnmarshal(bz, &st)
	return st.Params
}

// SetStakeUSb updates staked $USb into the BondReserve
func (k Keeper) SetStakeUSb(ctx sdk.Context, stake []types.StakeUSbParams) {
	store := ctx.KVStore(k.storeKey)
	bz := k.cdc.MustMarshal(&types.StakeUSb{Params: stake})
	store.Set(types.StakeUSbKey, bz)
}

// AddStakeUSb will add $USb into the BondReserve
func (k Keeper) AddStakeUSb(ctx sdk.Context, inCoin sdk.Coin, addr sdk.AccAddress) error {
	USbCoins := sdk.NewCoins(inCoin)
	stakerFound := false
	stakers := k.GetStakeUSb(ctx)

	//Stake USb into the BondReserve
	err := k.BankKeeper.SendCoinsFromAccountToModule(ctx, addr, types.BondReserve, USbCoins)
	if err != nil {
		return err
	}

	for num, staker := range stakers {
		if staker.StakeAddress == addr.String() {
			stakerFound = true
			newBalance := inCoin.Add(*staker.UsbStake)
			stakers[num].UsbStake = &newBalance
			break
		}
	}

	if !stakerFound {
		var newStaker types.StakeUSbParams
		newStaker.StakeAddress = addr.String()
		newStaker.UsbStake = &inCoin
		stakers = append(stakers, newStaker)
	}

	k.SetStakeUSb(ctx, stakers)

	return nil
}

// SubStakeUSb will substract $USb into the BondReserve
func (k Keeper) SubStakeUSb(ctx sdk.Context, inCoin sdk.Coin, addr sdk.AccAddress) error {
	stakers := k.GetStakeUSb(ctx)
	inCoins := sdk.NewCoins(inCoin)

	for num, staker := range stakers {
		if staker.StakeAddress == addr.String() {
			stakerUsbStake := *staker.UsbStake
			newBalance := stakerUsbStake.Sub(inCoin)

			//Only sub if we get 0 or positive
			if !newBalance.IsNegative() {

				//Unstake USb into the BondReserve
				err := k.BankKeeper.SendCoinsFromModuleToAccount(ctx, types.BondReserve, addr, inCoins)
				if err != nil {
					return err
				}

				stakers[num].UsbStake = &newBalance
				if stakers[num].UsbStake.IsZero() {
					stakers = append(stakers[:num], stakers[num+1:]...)
				}
			}
			break
		}
	}

	k.SetStakeUSb(ctx, stakers)

	return nil
}

// BurnStakeUSb will burn $USb into the stakers list
func (k Keeper) BurnStakeUSb(ctx sdk.Context, perc_map map[string]sdk.Dec, total sdk.Coin) {
	stakers := k.GetStakeUSb(ctx)
	totalDec := total.Amount.ToDec()

	for addr, perc := range perc_map {
		for num, staker := range stakers {
			if staker.StakeAddress == addr {
				subAmt := totalDec.Mul(perc).TruncateInt()
				subCoin := sdk.NewCoin(core.MicroUSbDenom, subAmt)
				err := k.BankKeeper.SendCoinsFromModuleToModule(ctx, types.BondReserve, types.ModuleName, sdk.NewCoins(subCoin))
				if err != nil {
					ctx.Logger().Error("Fail to send USb on BurnStakeUSb,", " error:", err)
				}
				err = k.BankKeeper.BurnCoins(ctx, types.ModuleName, sdk.NewCoins(subCoin))
				if err != nil {
					ctx.Logger().Error("Fail to burn USb on BurnStakeUSb,", " error:", err)
				}
				newBalance := staker.UsbStake.Sub(subCoin)
				stakers[num].UsbStake = &newBalance

				if stakers[num].UsbStake.IsZero() {
					stakers = append(stakers[:num], stakers[num+1:]...)
				}
			}
		}
	}

	k.SetStakeUSb(ctx, stakers)
}

//Convert $USa into $USb
func (k Keeper) ConvertUSaToUSb(inUSa sdk.Coin) sdk.Coin {
	//Hardcoded value
	USaToUSbPerc := sdk.NewDecWithPrec(99, 2)

	USbDec := sdk.NewDec(inUSa.Amount.Int64())
	USbDecPer := USbDec.Mul(USaToUSbPerc)
	USbInt := USbDecPer.TruncateInt()
	USbCoin := sdk.NewCoin(core.MicroUSbDenom, USbInt)

	return USbCoin
}

//Convert $CACAO to $USD and then to $USb
func (k Keeper) ConvertCACAOToUSb(ctx sdk.Context, inCACAO sdk.Coin) sdk.Coin {
	CACAOToUSDPerc, err := k.OracleKeeper.GetLunaExchangeRate(ctx, core.MicroUSDDenom)
	if err != nil {
		ctx.Logger().Error("fail to get exchange rate", "error", err)
	}

	CACAODec := sdk.NewDec(inCACAO.Amount.Int64())
	USD := CACAODec.Mul(CACAOToUSDPerc)

	USbInt := USD.TruncateInt()

	return sdk.NewCoin(core.MicroUSbDenom, USbInt)
}

//MintCoinsIntoBondReserve will mint the coins into the BondReserve
func (k Keeper) MintCoinIntoBondReserve(ctx sdk.Context, inCoin sdk.Coin) error {
	inCoins := sdk.NewCoins(inCoin)

	err := k.BankKeeper.MintCoins(ctx, types.ModuleName, inCoins)
	if err != nil {
		return err
	}
	err = k.BankKeeper.SendCoinsFromModuleToModule(ctx, types.ModuleName, types.BondReserve, inCoins)
	if err != nil {
		return err
	}

	return nil
}

//Distribute CACAO that is in the BondReserve
func (k Keeper) DistributeCACAOBondReserve(ctx sdk.Context) {
	bondReserveAddr := k.AccountKeeper.GetModuleAddress(types.BondReserve)
	bondReserveBalance := k.BankKeeper.GetBalance(ctx, bondReserveAddr, core.MicroLunaDenom)
	amountOfUSbOnBondReserve := k.BankKeeper.GetBalance(ctx, bondReserveAddr, core.MicroUSbDenom)
	_, err := k.OracleKeeper.GetLunaExchangeRate(ctx, core.MicroUSDDenom)
	cacaoPaided := sdk.NewCoin(core.MicroLunaDenom, sdk.NewInt(0))
	perc := make(map[string]sdk.Dec)

	//Distribute only if we have somenthing in the reserve and the exchange oracle is working
	if !bondReserveBalance.IsZero() && err == nil {

		stakersUSb := k.GetStakeUSb(ctx)
		for _, staker := range stakersUSb {
			//Get the amount of USb from the account
			bondBalance := *staker.UsbStake

			// Check if account has USb at least 100USb
			if !bondBalance.IsLT(sdk.NewCoin(staker.UsbStake.Denom, sdk.NewInt(core.MinMultipleBondReserve))) {
				bondReserveDec := sdk.NewDecCoinFromCoin(bondReserveBalance)
				bondAmount := bondBalance.Amount.ToDec()
				percentage := bondAmount.QuoInt64(amountOfUSbOnBondReserve.Amount.Int64())
				percentageCoins := bondReserveDec.Amount.Mul(percentage)
				sendCoin, _ := sdk.NewDecCoinFromDec(bondReserveBalance.Denom, percentageCoins).TruncateDecimal()

				address, err := sdk.AccAddressFromBech32(staker.StakeAddress)
				if err != nil {
					ctx.Logger().Error("fail to get address", address, " on DistributeCACAOBondReserve", "error", err)
				}

				err = k.BankKeeper.SendCoinsFromModuleToAccount(ctx, types.BondReserve, address, sdk.NewCoins(sendCoin))
				if err != nil {
					ctx.Logger().Error("fail to send", bondReserveBalance.Denom, " on DistributeCACAOBondReserve", "error", err)
				} else {
					perc[staker.StakeAddress] = percentage
					cacaoPaided = cacaoPaided.Add(sendCoin)
				}
			}
		}

		if !cacaoPaided.IsZero() {
			USbFromCACAO := k.ConvertCACAOToUSb(ctx, cacaoPaided)
			k.BurnStakeUSb(ctx, perc, USbFromCACAO)
		}
	}
}

//Distribute $USb that is in the BondReserve
func (k Keeper) DistributeUSbBondReserve(ctx sdk.Context) {
	accounts := k.AccountKeeper.GetAllAccounts(ctx)
	amountOfUSbOnChain := k.BankKeeper.GetSupply(ctx, core.MicroUSbDenom)
	amountOfUSbOnChainDec := sdk.NewDecCoinFromCoin(amountOfUSbOnChain)
	bondReserveAddr := k.AccountKeeper.GetModuleAddress(types.BondReserve)
	USbToMintPerc := sdk.NewDecWithPrec(0001, 4)
	USbToMint := amountOfUSbOnChainDec.Amount.Mul(USbToMintPerc)

	//Distribute only if we have at least MinMultipleBondReserve in the reserve
	if !amountOfUSbOnChain.IsLT(sdk.NewCoin(core.MicroUSbDenom, sdk.NewInt(core.MinMultipleBondReserve))) {
		stakersUSb := k.GetStakeUSb(ctx)

		//Iterate for all the available accounts
		for _, acc := range accounts {
			//Get the amount of USb from the account
			bondBalance := k.BankKeeper.GetBalance(ctx, acc.GetAddress(), core.MicroUSbDenom)

			// Check if account has USb and the acc is not the BondReserve
			if !bondBalance.IsZero() && !acc.GetAddress().Equals(bondReserveAddr) {
				bondAmount := bondBalance.Amount.ToDec()
				percentage := bondAmount.Quo(amountOfUSbOnChainDec.Amount)
				percentageCoins := USbToMint.Mul(percentage)

				sendCoin, _ := sdk.NewDecCoinFromDec(core.MicroUSbDenom, percentageCoins).TruncateDecimal()

				err := k.MintCoinIntoBondReserve(ctx, sendCoin)
				if err != nil {
					ctx.Logger().Error("fail to mint", sendCoin, " on DistributeUSbBondReserve", "error", err)
				}

				err = k.BankKeeper.SendCoinsFromModuleToAccount(ctx, types.BondReserve, acc.GetAddress(), sdk.NewCoins(sendCoin))
				if err != nil {
					ctx.Logger().Error("fail to send", sendCoin, " on DistributeUSbBondReserve", "error", err)
				}
			}
		}

		for _, staker := range stakersUSb {
			//Get the amount of USb from the account
			bondBalance := *staker.UsbStake

			// Check if account has USb at least 100USb
			if !bondBalance.IsLT(sdk.NewCoin(staker.UsbStake.Denom, sdk.NewInt(core.MinMultipleBondReserve))) {
				bondAmount := bondBalance.Amount.ToDec()
				percentage := bondAmount.Quo(amountOfUSbOnChainDec.Amount)
				percentageCoins := USbToMint.Mul(percentage)
				sendCoin, _ := sdk.NewDecCoinFromDec(core.MicroUSbDenom, percentageCoins).TruncateDecimal()

				address, err := sdk.AccAddressFromBech32(staker.StakeAddress)
				if err != nil {
					ctx.Logger().Error("fail to get address", address, " on DistributeUSbBondReserve", "error", err)
				}

				err = k.MintCoinIntoBondReserve(ctx, sendCoin)
				if err != nil {
					ctx.Logger().Error("fail to mint", sendCoin, " on DistributeUSbBondReserve", "error", err)
				}

				err = k.BankKeeper.SendCoinsFromModuleToAccount(ctx, types.BondReserve, address, sdk.NewCoins(sendCoin))
				if err != nil {
					ctx.Logger().Error("fail to send", sendCoin, " on DistributeUSbBondReserve", "error", err)
				}
			}
		}
	}

}
