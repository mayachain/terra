package cli

import (
	"strings"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"github.com/spf13/cobra"

	feeutils "github.com/terra-money/core/custom/auth/client/utils"
	"github.com/terra-money/core/x/market/types"
)

// GetTxCmd returns the transaction commands for this module
func GetTxCmd() *cobra.Command {
	marketTxCmd := &cobra.Command{
		Use:                        "market",
		Short:                      "Market transaction subcommands",
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	marketTxCmd.AddCommand(
		GetSwapCmd(),
		MintUSbCmd(),
		StakeUSbCmd(),
		UnStakeUSbCmd(),
	)

	return marketTxCmd
}

// GetSwapCmd will create and send a MsgSwap
func GetSwapCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "swap [offer-coin] [ask-denom] [to-address]",
		Args:  cobra.RangeArgs(2, 3),
		Short: "Atomically swap currencies at their target exchange rate",
		Long: strings.TrimSpace(`
Swap the offer-coin to the ask-denom currency at the oracle's effective exchange rate. 

$ terrad market swap "1000ukrw" "uusd"

The to-address can be specified. A default to-address is trader.

$ terrad market swap "1000ukrw" "uusd" "terra1..."
`),
		RunE: func(cmd *cobra.Command, args []string) error {

			// Swap currently disable until MAYA 3.0
			return types.ErrSwapDisable

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			// Generate transaction factory for gas simulation
			txf := tx.NewFactoryCLI(clientCtx, cmd.Flags())

			offerCoinStr := args[0]
			offerCoin, err := sdk.ParseCoinNormalized(offerCoinStr)
			if err != nil {
				return err
			}

			askDenom := args[1]
			fromAddress := clientCtx.GetFromAddress()

			var msg sdk.Msg
			if len(args) == 3 {
				toAddress, err := sdk.AccAddressFromBech32(args[2])
				if err != nil {
					return err
				}

				msg = types.NewMsgSwapSend(fromAddress, toAddress, offerCoin, askDenom)
				if err = msg.ValidateBasic(); err != nil {
					return err
				}

				if !clientCtx.GenerateOnly && txf.Fees().IsZero() {
					// estimate tax and gas
					stdFee, err := feeutils.ComputeFeesWithCmd(clientCtx, cmd.Flags(), msg)

					if err != nil {
						return err
					}

					// override gas and fees
					txf = txf.
						WithFees(stdFee.Amount.String()).
						WithGas(stdFee.Gas).
						WithSimulateAndExecute(false).
						WithGasPrices("")
				}
			} else {
				msg = types.NewMsgSwap(fromAddress, offerCoin, askDenom)
				if err = msg.ValidateBasic(); err != nil {
					return err
				}
			}

			// build and sign the transaction, then broadcast to Tendermint
			return tx.GenerateOrBroadcastTxWithFactory(clientCtx, txf, msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

// MintUSbCmd will create and send a MsgMintUSb
func MintUSbCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "mint-usb [amount]",
		Args:  cobra.ExactArgs(1),
		Short: "Burn $USa and mint $USb",
		Long: strings.TrimSpace(`
If you have $USa in your account you can mint it into $USb. 

$ terrad market mint-usb 1000uusa --from "address"`),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			// Generate transaction factory for gas simulation
			txf := tx.NewFactoryCLI(clientCtx, cmd.Flags())

			amountCoins := args[0]
			amountCoin, err := sdk.ParseCoinNormalized(amountCoins)
			if err != nil {
				return err
			}

			fromAddress := clientCtx.GetFromAddress()

			msg := types.NewMsgMintUSb(fromAddress, amountCoin)
			if err = msg.ValidateBasic(); err != nil {
				return err
			}

			if !clientCtx.GenerateOnly && txf.Fees().IsZero() {
				// estimate tax and gas
				stdFee, err := feeutils.ComputeFeesWithCmd(clientCtx, cmd.Flags(), msg)

				if err != nil {
					return err
				}

				// override gas and fees
				txf = txf.
					WithFees(stdFee.Amount.String()).
					WithGas(stdFee.Gas).
					WithSimulateAndExecute(false).
					WithGasPrices("")
			}

			// build and sign the transaction, then broadcast to Tendermint
			return tx.GenerateOrBroadcastTxWithFactory(clientCtx, txf, msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

// StakeUSbCmd will create and send a MsgStakeUSb
func StakeUSbCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "stake-usb [amount]",
		Args:  cobra.ExactArgs(1),
		Short: "Stake $USb into the BondReserve",
		Long: strings.TrimSpace(`
If you have $USb you can stake it into the BondReserve in order to get fees. 

Note that you can only unstake at least 10000000000uusb.

$ terrad market stake-usb 10000000000uusb --from "address"`),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			// Generate transaction factory for gas simulation
			txf := tx.NewFactoryCLI(clientCtx, cmd.Flags())

			amountCoins := args[0]
			amountCoin, err := sdk.ParseCoinNormalized(amountCoins)
			if err != nil {
				return err
			}

			fromAddress := clientCtx.GetFromAddress()

			msg := types.NewMsgStakeUSb(fromAddress, amountCoin)
			if err = msg.ValidateBasic(); err != nil {
				return err
			}

			if !clientCtx.GenerateOnly && txf.Fees().IsZero() {
				// estimate tax and gas
				stdFee, err := feeutils.ComputeFeesWithCmd(clientCtx, cmd.Flags(), msg)

				if err != nil {
					return err
				}

				// override gas and fees
				txf = txf.
					WithFees(stdFee.Amount.String()).
					WithGas(stdFee.Gas).
					WithSimulateAndExecute(false).
					WithGasPrices("")
			}

			// build and sign the transaction, then broadcast to Tendermint
			return tx.GenerateOrBroadcastTxWithFactory(clientCtx, txf, msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}

// UnStakeUSbCmd will create and send a MsgUnStakeUSb
func UnStakeUSbCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "unstake-usb [amount]",
		Args:  cobra.ExactArgs(1),
		Short: "Untake $USb from BondReserve",
		Long: strings.TrimSpace(`
If you have $USb in the BondReserve you can unstake it to get back your amount. 

Note that you can only unstake at least 1000000000uusb.

$ terrad market unstake-usb 1000000000uusb --from "address"`),
		RunE: func(cmd *cobra.Command, args []string) error {

			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			// Generate transaction factory for gas simulation
			txf := tx.NewFactoryCLI(clientCtx, cmd.Flags())

			amountCoins := args[0]
			amountCoin, err := sdk.ParseCoinNormalized(amountCoins)
			if err != nil {
				return err
			}

			fromAddress := clientCtx.GetFromAddress()

			msg := types.NewMsgUnStakeUSb(fromAddress, amountCoin)
			if err = msg.ValidateBasic(); err != nil {
				return err
			}

			if !clientCtx.GenerateOnly && txf.Fees().IsZero() {
				// estimate tax and gas
				stdFee, err := feeutils.ComputeFeesWithCmd(clientCtx, cmd.Flags(), msg)

				if err != nil {
					return err
				}

				// override gas and fees
				txf = txf.
					WithFees(stdFee.Amount.String()).
					WithGas(stdFee.Gas).
					WithSimulateAndExecute(false).
					WithGasPrices("")
			}

			// build and sign the transaction, then broadcast to Tendermint
			return tx.GenerateOrBroadcastTxWithFactory(clientCtx, txf, msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
