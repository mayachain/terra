//nolint:deadcode,unused
//DONTCOVER
package exported

import "github.com/terra-money/core/x/market/types"

type (
	MsgSwap       = types.MsgSwap
	MsgSwapSend   = types.MsgSwapSend
	MsgMintUSb    = types.MsgMintUSb
	MsgStakeUSb   = types.MsgStakeUSb
	MsgUnStakeUSb = types.MsgUnStakeUSb
)
