package types

import (
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
)

// Market errors
var (
	ErrRecursiveSwap     = sdkerrors.Register(ModuleName, 2, "recursive swap")
	ErrNoEffectivePrice  = sdkerrors.Register(ModuleName, 3, "no price registered with oracle")
	ErrNoUSa             = sdkerrors.Register(ModuleName, 4, "you can only mint USa into USb")
	ErrNoStakeUSbDenom   = sdkerrors.Register(ModuleName, 5, "you can only stake USb in the form of uusb")
	ErrNoUnStakeUSbDenom = sdkerrors.Register(ModuleName, 6, "you can only unstake USb in the form of uusb")
	ErrMinStakeUSb       = sdkerrors.Register(ModuleName, 7, "you can stake at least 100USb = 10000000000uusb")
	ErrMinUnstakeUSb     = sdkerrors.Register(ModuleName, 8, "you can unstake at least 10USb = 1000000000uusb")
	ErrSwapDisable       = sdkerrors.Register(ModuleName, 9, "swap feature is currently disable, this will be available on MAYA 3.0")
)
