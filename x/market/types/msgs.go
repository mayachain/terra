package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	core "github.com/terra-money/core/types"
)

// ensure Msg interface compliance at compile time
var (
	_ sdk.Msg = &MsgSwap{}
	_ sdk.Msg = &MsgSwapSend{}
	_ sdk.Msg = &MsgMintUSb{}
	_ sdk.Msg = &MsgStakeUSb{}
	_ sdk.Msg = &MsgUnStakeUSb{}
)

// market message types
const (
	TypeMsgSwap       = "swap"
	TypeMsgSwapSend   = "swap_send"
	TypeMsgMintUSb    = "mint_usb"
	TypeMsgStakeUSb   = "stake_usb"
	TypeMsgUnStakeUSb = "unstake_usb"
)

//--------------------------------------------------------
//--------------------------------------------------------

// NewMsgSwap creates a MsgSwap instance
func NewMsgSwap(traderAddress sdk.AccAddress, offerCoin sdk.Coin, askCoin string) *MsgSwap {
	return &MsgSwap{
		Trader:    traderAddress.String(),
		OfferCoin: offerCoin,
		AskDenom:  askCoin,
	}
}

// Route Implements Msg
func (msg MsgSwap) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgSwap) Type() string { return TypeMsgSwap }

// GetSignBytes Implements Msg
func (msg MsgSwap) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners Implements Msg
func (msg MsgSwap) GetSigners() []sdk.AccAddress {
	trader, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{trader}
}

// ValidateBasic Implements Msg
func (msg MsgSwap) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid trader address (%s)", err)
	}

	if msg.OfferCoin.Amount.LTE(sdk.ZeroInt()) || msg.OfferCoin.Amount.BigInt().BitLen() > 100 {
		return sdkerrors.Wrap(sdkerrors.ErrInvalidCoins, msg.OfferCoin.String())
	}

	if msg.OfferCoin.Denom == msg.AskDenom {
		return sdkerrors.Wrap(ErrRecursiveSwap, msg.AskDenom)
	}

	return nil
}

// NewMsgSwapSend conducts market swap and send all the result coins to recipient
func NewMsgSwapSend(fromAddress sdk.AccAddress, toAddress sdk.AccAddress, offerCoin sdk.Coin, askCoin string) *MsgSwapSend {
	return &MsgSwapSend{
		FromAddress: fromAddress.String(),
		ToAddress:   toAddress.String(),
		OfferCoin:   offerCoin,
		AskDenom:    askCoin,
	}
}

// Route Implements Msg
func (msg MsgSwapSend) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgSwapSend) Type() string { return TypeMsgSwapSend }

// GetSignBytes Implements Msg
func (msg MsgSwapSend) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners Implements Msg
func (msg MsgSwapSend) GetSigners() []sdk.AccAddress {
	from, err := sdk.AccAddressFromBech32(msg.FromAddress)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{from}
}

// ValidateBasic Implements Msg
func (msg MsgSwapSend) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.FromAddress)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid from address (%s)", err)
	}

	_, err = sdk.AccAddressFromBech32(msg.ToAddress)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid to address (%s)", err)
	}

	if msg.OfferCoin.Amount.LTE(sdk.ZeroInt()) || msg.OfferCoin.Amount.BigInt().BitLen() > 100 {
		return sdkerrors.Wrap(sdkerrors.ErrInvalidCoins, msg.OfferCoin.String())
	}

	if msg.OfferCoin.Denom == msg.AskDenom {
		return sdkerrors.Wrap(ErrRecursiveSwap, msg.AskDenom)
	}

	return nil
}

// NewMsgMintUSb creates a MsgSwap instance
func NewMsgMintUSb(traderAddress sdk.AccAddress, coin sdk.Coin) *MsgMintUSb {
	return &MsgMintUSb{
		Trader:  traderAddress.String(),
		UsaCoin: coin,
	}
}

// Route Implements Msg
func (msg MsgMintUSb) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgMintUSb) Type() string { return TypeMsgMintUSb }

// GetSignBytes Implements Msg
func (msg MsgMintUSb) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners Implements Msg
func (msg MsgMintUSb) GetSigners() []sdk.AccAddress {
	trader, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{trader}
}

// ValidateBasic Implements Msg
func (msg MsgMintUSb) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid trader address (%s)", err)
	}

	if msg.UsaCoin.Amount.LTE(sdk.ZeroInt()) || msg.UsaCoin.Amount.BigInt().BitLen() > 100 {
		return sdkerrors.Wrap(sdkerrors.ErrInvalidCoins, msg.UsaCoin.String())
	}

	if msg.UsaCoin.Denom != core.MicroUSaDenom {
		return sdkerrors.Wrap(ErrNoUSa, core.MicroUSaDenom)
	}

	return nil
}

// NewMsgStakeUSb creates a MsgSwap instance
func NewMsgStakeUSb(traderAddress sdk.AccAddress, coin sdk.Coin) *MsgStakeUSb {
	return &MsgStakeUSb{
		Trader:         traderAddress.String(),
		OfferStakeCoin: coin,
	}
}

// Route Implements Msg
func (msg MsgStakeUSb) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgStakeUSb) Type() string { return TypeMsgStakeUSb }

// GetSignBytes Implements Msg
func (msg MsgStakeUSb) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners Implements Msg
func (msg MsgStakeUSb) GetSigners() []sdk.AccAddress {
	trader, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{trader}
}

// ValidateBasic Implements Msg
func (msg MsgStakeUSb) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid trader address (%s)", err)
	}

	if msg.OfferStakeCoin.Amount.LTE(sdk.ZeroInt()) || msg.OfferStakeCoin.Amount.BigInt().BitLen() > 100 {
		return sdkerrors.Wrap(sdkerrors.ErrInvalidCoins, msg.OfferStakeCoin.String())
	}

	if msg.OfferStakeCoin.Denom != core.MicroUSbDenom {
		return sdkerrors.Wrap(ErrNoStakeUSbDenom, core.MicroUSbDenom)
	}

	if msg.OfferStakeCoin.Amount.LT(sdk.NewInt(core.MinStakeUSb)) {
		return sdkerrors.Wrap(ErrMinStakeUSb, msg.OfferStakeCoin.String())
	}

	return nil
}

// NewMsgStakeUSb creates a MsgSwap instance
func NewMsgUnStakeUSb(traderAddress sdk.AccAddress, coin sdk.Coin) *MsgUnStakeUSb {
	return &MsgUnStakeUSb{
		Trader:           traderAddress.String(),
		OfferUnstakeCoin: coin,
	}
}

// Route Implements Msg
func (msg MsgUnStakeUSb) Route() string { return RouterKey }

// Type implements sdk.Msg
func (msg MsgUnStakeUSb) Type() string { return TypeMsgUnStakeUSb }

// GetSignBytes Implements Msg
func (msg MsgUnStakeUSb) GetSignBytes() []byte {
	return sdk.MustSortJSON(ModuleCdc.MustMarshalJSON(&msg))
}

// GetSigners Implements Msg
func (msg MsgUnStakeUSb) GetSigners() []sdk.AccAddress {
	trader, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		panic(err)
	}

	return []sdk.AccAddress{trader}
}

// ValidateBasic Implements Msg
func (msg MsgUnStakeUSb) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Trader)
	if err != nil {
		return sdkerrors.Wrapf(sdkerrors.ErrInvalidAddress, "Invalid trader address (%s)", err)
	}

	if msg.OfferUnstakeCoin.Amount.LTE(sdk.ZeroInt()) || msg.OfferUnstakeCoin.Amount.BigInt().BitLen() > 100 {
		return sdkerrors.Wrap(sdkerrors.ErrInvalidCoins, msg.OfferUnstakeCoin.String())
	}

	if msg.OfferUnstakeCoin.Denom != core.MicroUSbDenom {
		return sdkerrors.Wrap(ErrNoUnStakeUSbDenom, core.MicroUSbDenom)
	}

	if msg.OfferUnstakeCoin.Amount.LT(sdk.NewInt(core.MinUnstakeUSb)) {
		return sdkerrors.Wrap(ErrMinUnstakeUSb, msg.OfferUnstakeCoin.String())
	}

	return nil
}
